# Changelog

## 3.4.0 - 2024-11-27

### Added

- Installable en tant que package Composer

### Changed

- Utilisation du collecteur `HtmlTag` (refactor).
- Compatible SPIP 5.0.0-dev
