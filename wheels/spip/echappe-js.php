<?php

/**
 * Fonctions utiles pour la wheel echappe-js
 *
 * @SPIP\Textwheel\Wheel\SPIP\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function echappe_anti_xss($match) {
	static $safehtml;

	if (!is_array($match) || !strlen((string) $match[0])) {
		return '';
	}
	$texte = &$match[0];

	// on echappe les urls data: javascript: et tout ce qui ressemble
	if (
		str_contains((string) $texte, ':') && preg_match(',(data|script)\s*:,iS', (string) $texte)
	) {
		$texte = nl2br(spip_htmlspecialchars($texte, ENT_NOQUOTES));
	} elseif (
		// on echappe si on a possiblement un attribut onxxx et que ca passe pas dans safehtml
		stripos((string) $texte, 'on') !== false && preg_match(",\bon\w+\s*=,i", (string) $texte)
	) {
		if (!isset($safehtml)) {
			$safehtml = charger_fonction('safehtml', 'inc', true);
		}
		if (!$safehtml || strlen((string) $safehtml($texte)) !== strlen((string) $texte)) {
			$texte = nl2br(spip_htmlspecialchars($texte, ENT_NOQUOTES));
		}
	}

	if (!str_contains((string) $texte, '<')) {
		$texte = "<code class=\"echappe-js\">$texte</code>";
	}

	return $texte;
}
